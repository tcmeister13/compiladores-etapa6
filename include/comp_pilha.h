/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>


typedef struct pilha comp_pilha_t;

typedef struct pilha
{
	int info;             /* REMOVER E INCLUIR AS INFOS DEPOIS */
    comp_pilha_t* prox;   /* Pr�ximo nodo da pilha  */
};

comp_pilha_t* pilha_init();
comp_pilha_t* pilha_push(comp_pilha_t *p, int info);
comp_pilha_t* pilha_pop(comp_pilha_t *p);
void          pilha_imprime(comp_pilha_t *p);
