/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/

typedef struct TIPO_PARAM tipo_param;
typedef struct TIPO_DICIONARIO comp_dict_t;
typedef struct TIPO_FUNCAO tipo_funcao;
typedef struct TIPO_VETOR tipo_vetor;
typedef struct TIPO_TAM_DIMENSAO tam_dimensao;

//tipo dicionario: lista encadeada para entradas
struct TIPO_DICIONARIO
{
    char *chave; //conteudo da entrada
    int linha;	//linha de ocorrência
    int tipo;	//tipo da entrada
    int tamanho; //tamanho em bytes
    int tipo_id; //tipo de identificador
    tipo_funcao* funcao; //informações se identificador funcao
    tipo_vetor* vetor;
    int deslocamento;
    comp_dict_t * prox; //proxima entrada
};

struct TIPO_FUNCAO
{
   int tipo_retorno; //tipo de retorno da função
   char* label_cod;
   tipo_param* param; //lista de parametros
   int n_param; //numero de parametros da funcao
};

struct TIPO_PARAM
{
   int tipo;
   tipo_param* prox;
};

struct TIPO_VETOR
{
  int dimensao;
  int tamanho;
  tam_dimensao* dimensoes;
};

struct TIPO_TAM_DIMENSAO
{
  int tamanho;
  tam_dimensao* prox;
};

comp_dict_t* dicionario_AdicionaEntrada (char* Chave, int linha, int tipo, comp_dict_t* Dicionario);
void dicionario_imprime(comp_dict_t* dicio);
comp_dict_t* dicionario_Alocar ();
comp_dict_t* dicionario_CriaCopiaUnica (comp_dict_t* dicionario);
comp_dict_t* dicionario_CriaCopiaCompleta (comp_dict_t* dicionario);
comp_dict_t* dicionario_LocalizarEntrada (char* Chave, comp_dict_t* Dicionario);
void dicionario_Liberar(comp_dict_t* dicio);

tipo_vetor* dicionario_novaDimensao(int fator, tipo_vetor* anterior);

tam_dimensao* dicionario_incluiDimensaoFinal(int tam, tam_dimensao* add);
tipo_param* dicionario_incluiParametroFinal(int tipo, tipo_param* add);
comp_dict_t* dicionario_fatorVetor(tipo_vetor* fator, comp_dict_t* dicio);

int dicionario_elementosDimensaoK(int k,comp_dict_t* vetor);



