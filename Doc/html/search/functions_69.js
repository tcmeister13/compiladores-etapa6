var searchData=
[
  ['if',['if',['../lexer_8c.html#ad4a65b873df5c05570846b5413b41dfd',1,'lexer.c']]],
  ['iks_5fast_5fpara_5fopcode',['IKS_AST_para_opCode',['../define_8h.html#adcab94fb843fab1d03a086c608528542',1,'IKS_AST_para_opCode(int ast):&#160;define.c'],['../define_8c.html#adcab94fb843fab1d03a086c608528542',1,'IKS_AST_para_opCode(int ast):&#160;define.c']]],
  ['iks_5fast_5fpara_5ftexto',['IKS_AST_para_texto',['../define_8c.html#a0110d384894f46f6b9e5ca656da2bb1a',1,'define.c']]],
  ['iks_5ftipo_5fpara_5ftexto',['IKS_TIPO_para_texto',['../define_8h.html#a5819226a1e113a4aed2f00e40144f26f',1,'IKS_TIPO_para_texto(int iks):&#160;define.c'],['../define_8c.html#ade1280974b84bd473b529a7791374471',1,'IKS_TIPO_para_texto(int iks):&#160;define.c']]],
  ['imprime_5ferro',['Imprime_Erro',['../define_8h.html#aeb98f61bb312cbcad8f48d9e0766b008',1,'Imprime_Erro(int linha, int erro):&#160;define.c'],['../define_8c.html#aeb98f61bb312cbcad8f48d9e0766b008',1,'Imprime_Erro(int linha, int erro):&#160;define.c']]],
  ['inferencia_5fcom_5ftipo',['inferencia_com_tipo',['../comp__tree_8h.html#a45a699cedfa34f7dd0092b2ea6e3e4e3',1,'inferencia_com_tipo(int tipo, int *filhos, int n):&#160;comp_tree.c'],['../comp__tree_8c.html#a45a699cedfa34f7dd0092b2ea6e3e4e3',1,'inferencia_com_tipo(int tipo, int *filhos, int n):&#160;comp_tree.c']]],
  ['inferencia_5ftodos_5ftipo',['inferencia_todos_tipo',['../comp__tree_8h.html#a339e31c5cc1238b7eabef8af8ec7e363',1,'inferencia_todos_tipo(int tipo, int *filhos, int n):&#160;comp_tree.c'],['../comp__tree_8c.html#a339e31c5cc1238b7eabef8af8ec7e363',1,'inferencia_todos_tipo(int tipo, int *filhos, int n):&#160;comp_tree.c']]],
  ['isatty',['isatty',['../lexer_8c.html#ab4155ffea05dab2dafae68fd88a0517f',1,'lexer.c']]]
];
