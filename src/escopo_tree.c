/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include "comp_dict.h"
#include "comp_tree.h"
#include "escopo_tree.h"
#include "comp_list.h"
#include "define.h"

extern int linha;
extern escopo_tree_t* tabela_simbolos_escopo;


/**
 * Gerencia a liberação do escopo
 * name: escopo_Liberar
 * @param  escopo_prog: escopo onde a liberação deverá atuar
 * @return
 *
 */
void escopo_Liberar(escopo_tree_t* escopo_prog)
{
	if(escopo_prog!=NULL)
	{
		dicionario_Liberar(escopo_prog->escopo_global);

		escopo_local* aux = escopo_prog->escopos_locais;
		escopo_local* liberar = escopo_prog->escopos_locais;
		while(aux!=NULL)
		{
			aux = aux->prox;
			dicionario_Liberar(liberar->escopo_local);
			free(liberar);
			liberar=aux;
		}

		free(escopo_prog);
	}

}

/**
 * Cria escopo global
 * name: escopo_criaEscopoGlobal
 * @param
 * @return escopo_tree_t alocado
 *
 */
escopo_tree_t* escopo_criaEscopoGlobal()
{
	escopo_tree_t* globale = (escopo_tree_t*) malloc(sizeof(escopo_tree_t));
	globale->base_global = 4096;
	globale->escopo_global = NULL;
	globale->escopos_locais = NULL;
	return globale;
}

/**
 * Cria um novo escopo local na arvore de escopos (torna como primeiro filho para posteriores inserções)
 * name: escopo_criaEscopoLocal
 * @param escopo_prog: onde deve ser criado escopo local
 * @return escopo atualizado
 *
 */
escopo_tree_t* escopo_criaEscopoLocal(escopo_tree_t* escopo_prog)
{
	escopo_local* novo_local = (escopo_local*) malloc(sizeof(escopo_local));
	
	novo_local->prox=escopo_prog->escopos_locais;
	novo_local->chave_funcao = NULL;
	novo_local->escopo_local = NULL;
	//Assumindo que o retornado tem tamanho 6
	novo_local->base_local=10 ;
	escopo_prog->escopos_locais=novo_local;
	return escopo_prog;
}

/**
 * Recalcula o deslocamento global em relação ao novo escopo inserido
 * name: escopo_deslocamentoGlobal
 * @param global: dicionario
 * @param escopo_prog: novo escopo
 * @return parametro deslocamento do dicionario global atualizado
 *
 */
comp_dict_t* escopo_deslocamentoGlobal(comp_dict_t* global, escopo_tree_t* escopo_prog)
{
	if(escopo_prog->escopo_global==NULL)
		global->deslocamento = 0;
	else
		global->deslocamento = escopo_prog->escopo_global->deslocamento + escopo_prog->escopo_global->tamanho;

	return global;
}

/**
 * Recalcula o deslocamento local
 * name: escopo_deslocamentoLocal
 * @param local: dicionario do escopo local
 * @param escopo_prog: novo escopo
 * @return parametro deslocamento do dicionario local atualizado
 *
 */
comp_dict_t* escopo_deslocamentoLocal(comp_dict_t* local, escopo_tree_t* escopo_prog)
{
	if(escopo_prog->escopos_locais->escopo_local==NULL)
	{
	
		//Começo do deslocamento do local em 0
		local->deslocamento = 0;
	}
	else
		local->deslocamento = escopo_prog->escopos_locais->escopo_local->deslocamento + escopo_prog->escopos_locais->escopo_local->tamanho;

	return local;
}

/**
 * Insere novo escopo global
 * name: escopo_insereEscopoGlobal
 * @param escopo_prog: onde deve ser inserido
 * @param novo_global: nova entrada de dicionario para escopo global
 * @return escopo atualizado
 *
 */
escopo_tree_t* escopo_insereEscopoGlobal(escopo_tree_t* escopo_prog, comp_dict_t* novo_global, int tipo_id)
{
	comp_dict_t* busca = dicionario_LocalizarEntrada(novo_global->chave,escopo_prog->escopo_global);
	if(escopo_prog->escopo_global==NULL || busca ==NULL)
	{
		novo_global = escopo_deslocamentoGlobal(novo_global,escopo_prog);
		novo_global->prox=escopo_prog->escopo_global;
		escopo_prog->escopo_global=novo_global;
		escopo_prog->escopo_global->tipo_id=tipo_id;

		return escopo_prog;
	}
	else
	{
		dicionario_Liberar(novo_global);
		escopo_Liberar(escopo_prog);
		dicionario_Liberar(busca);
		Imprime_Erro(linha,IKS_ERROR_DECLARED);
		exit(IKS_ERROR_DECLARED);
		return NULL;
	}
}

/**
 * Insere no escopo global uma funcao, atualizando seu tipo e alocando estrutura funcao para salvar parametros
 * name: escopo_insereFuncaoEscopoGlobal
 * @param escopo_prog: onde deve ser inserida a funcao
 * @param novo_global: entrada da funcao a ser inserida
 * @param tipo_retorno: tipo de retorno da funcao a ser inserida
 * @return escopo atualizado
 *
 */
escopo_tree_t* escopo_insereFuncaoEscopoGlobal(escopo_tree_t* escopo_prog, comp_dict_t* novo_global, comp_dict_t* params)
{


	novo_global->tipo_id = TIPO_ID_FUNCAO;
	novo_global->funcao = (tipo_funcao*) malloc(sizeof(tipo_funcao));
	novo_global->funcao->tipo_retorno = novo_global->tipo;
	novo_global->funcao->param = NULL;
	novo_global->funcao->n_param = 0;
	novo_global->funcao->label_cod=NULL;
	novo_global->prox = NULL;

	comp_dict_t* busca = dicionario_LocalizarEntrada(novo_global->chave,escopo_prog->escopo_global);
	if(escopo_prog->escopo_global==NULL || busca ==NULL)
	{
		novo_global = escopo_deslocamentoGlobal(novo_global,escopo_prog);
		novo_global->prox=escopo_prog->escopo_global;
		escopo_prog->escopo_global=novo_global;
		escopo_prog = escopo_criaEscopoLocal(escopo_prog);

		comp_dict_t* aux=params;
		while(aux!=NULL && escopo_prog!=NULL)
		{
			comp_dict_t* copia = dicionario_CriaCopiaUnica(aux);
			escopo_prog = escopo_insereEscopoLocal(escopo_prog,copia,TIPO_ID_VARIAVEL);
			escopo_prog->escopos_locais->chave_funcao = novo_global->chave;
			aux=aux->prox;
		}


		if(escopo_prog!=NULL)
		{
			aux = params;
			while(aux!=NULL)
			{
				escopo_prog = escopo_insereParametrosFuncao(escopo_prog,aux->tipo);
				aux = aux->prox;
			}
			dicionario_Liberar(params);

			return escopo_prog;
		}

		dicionario_Liberar(params);
	}


	dicionario_Liberar(busca);
	escopo_Liberar(escopo_prog);
	dicionario_Liberar(novo_global);
	dicionario_Liberar(params);
	Imprime_Erro(linha,IKS_ERROR_DECLARED);
	exit(IKS_ERROR_DECLARED);
	return NULL;



}

/**
 * Insere na estrutura funcao um novo tipo de param da funcao atual (primeiro no escopo global)
 * name: escopo_insereParametrosFuncao
 * @param escopo_prog: onde deve ser inserido o novo tipo de parametro
 * @param tipo_param: tipo do parametro da funcao
 * @return escopo atualizado
 *
 */
escopo_tree_t* escopo_insereParametrosFuncao(escopo_tree_t* escopo_prog, int tipoparam)
{
	tipo_param* novo = (tipo_param*) malloc(sizeof(tipo_param));
	novo->tipo = tipoparam;
	novo->prox = NULL;

	tipo_param* aux = escopo_prog->escopo_global->funcao->param;

	if(aux==NULL)
		escopo_prog->escopo_global->funcao->param=novo;
	else
	{
		while(aux->prox!=NULL)
			aux=aux->prox;

		aux->prox = novo;
	}

	escopo_prog->escopo_global->funcao->n_param++;

	return escopo_prog;
}


/**
 * Insere no primeiro filho de escopos locais um novo simbolo
 * name: escopo_insereEscopoLocal
 * @param escopo_prog: onde deve ser inserido
 * @param novo_local: nova entrada de dicionario para escopo local
 * @return escopo atualizado
 *
 */
escopo_tree_t* escopo_insereEscopoLocal(escopo_tree_t* escopo_prog, comp_dict_t* novo_local, int tipo_id)
{
	comp_dict_t* busca = dicionario_LocalizarEntrada(novo_local->chave,escopo_prog->escopos_locais->escopo_local);
	if(escopo_prog->escopos_locais->escopo_local == NULL || busca==NULL)
	{
		novo_local = escopo_deslocamentoLocal(novo_local,escopo_prog);
		novo_local->prox = escopo_prog->escopos_locais->escopo_local;
		escopo_prog->escopos_locais->escopo_local=novo_local;
		escopo_prog->escopos_locais->escopo_local->tipo_id=tipo_id;
		return escopo_prog;

	}
	else
	{
		dicionario_Liberar(novo_local);
		escopo_Liberar(escopo_prog);
		dicionario_Liberar(busca);
		Imprime_Erro(linha,IKS_ERROR_DECLARED);
		exit(IKS_ERROR_DECLARED);
		return NULL;
	}

}

/**
 * Insere um proximo elemento no escopo
 * name: escopo_insereParametro
 * @param novo: onde deve ser inserido o novo elemento como proximo
 * @param params: novo elemento a ser inserido como proximo de novo
 * @return escopo com proximo elemento inserido
 *
 */
comp_dict_t* escopo_insereParametro(comp_dict_t* novo, comp_dict_t* params)
{
	novo->prox = params;
	return novo;
}

/**
 * Verifica se há uma entrada no escopo (no local atual ou no global)
 * name: escopo_localizarEm_LocalAtual_Global
 * @param escopo_prog: onde deve ser feita a busca
 * @param Chave: chave da entrada a ser buscada
 * @return entrada correspondente à busca, sendo NULL caso não encontre nenhuma
 *
 */
comp_dict_t* escopo_localizarEm_LocalAtual_Global(escopo_tree_t* escopo_prog, char* Chave)
{
	escopo_tree_t* aux = escopo_prog;
	comp_dict_t* auxd = escopo_prog->escopos_locais->escopo_local; //dicionario_CriaCopiaCompleta(escopo_prog->escopos_locais->escopo_local);
	comp_dict_t* entrada = dicionario_LocalizarEntrada(Chave, auxd);
	//dicionario_Liberar(auxd);
	if(entrada==NULL)
	{
		comp_dict_t* auxg = escopo_prog->escopo_global; //dicionario_CriaCopiaCompleta(aux->escopo_global);
		comp_dict_t* entrada2 = dicionario_LocalizarEntrada(Chave,auxg);

		//dicionario_Liberar(auxg);

		return entrada2;
	}
	else
		return entrada;
}


//se local ==1, se global ==2
comp_dict_t* escopo_Local_Global(escopo_tree_t* escopo_prog,char* Chave)
{
	//escopo_imprime(escopo_prog);
	escopo_tree_t* aux = escopo_prog;
	comp_dict_t* auxd = escopo_prog->escopos_locais->escopo_local; //dicionario_CriaCopiaCompleta(escopo_prog->escopos_locais->escopo_local);
	comp_dict_t* entrada = dicionario_LocalizarEntrada(Chave, auxd);
	//dicionario_Liberar(auxd);
	if(entrada==NULL)
	{
		comp_dict_t* auxg = escopo_prog->escopo_global; //dicionario_CriaCopiaCompleta(aux->escopo_global);
		comp_dict_t* entrada2 = dicionario_LocalizarEntrada(Chave,auxg);

		//dicionario_Liberar(auxg);
	
		dicionario_Liberar(entrada2);
		return 2;
	}
	else
	{
		dicionario_Liberar(entrada);
		return 1;
	}
}

/**
 * Verifica qual o erro no uso de um identificador (apenas mostra o erro, esta ja tenha sido detectado)
 * name: escopo_usoVariavel
 * @param arv: candidato a possuir erro (se ta usando como variavel ao inves de vetor, etc)
 * @param tipo_id: tipo que deveria ser igual ao do nodo em arv
 * @return
 *
 */
void escopo_usoVariavel(comp_tree_t* arv, int tipo_id)
{
	if(arv!=NULL)
	{
		if(arv->entrada->tipo_id!=tipo_id)
		{

			switch(arv->entrada->tipo_id)
			{
				case TIPO_ID_FUNCAO:
				{
					Imprime_Erro(linha,IKS_ERROR_FUNCTION);
					exit(IKS_ERROR_FUNCTION);
				};break;
				case TIPO_ID_VETOR:
				{
					Imprime_Erro(linha,IKS_ERROR_VECTOR);
					exit(IKS_ERROR_VECTOR);
				};break;
				case TIPO_ID_VARIAVEL:
				{
					Imprime_Erro(linha,IKS_ERROR_VARIABLE);
					exit(IKS_ERROR_VARIABLE);
				};break;
			}

			escopo_Liberar(tabela_simbolos_escopo);
			arvore_Liberar(arv);


		}

	}


}


/**
 * Imprime na tela conteudo do escopo
 * name: escopo_imprime
 * @param escopo_prog: escopo que deverá ser impresso
 * @return
 *
 */
void escopo_imprime(escopo_tree_t* escopo_prog)
{
	if(escopo_prog!=NULL)
	{
		printf("### TABELA DE SIMBOLOS ### \n");
		printf("## ESCOPO GLOBAL ##\n");
		dicionario_imprime(escopo_prog->escopo_global);
		printf("## ##\n");
		escopo_local* aux = escopo_prog->escopos_locais;
		while(aux!=NULL)
		{
			printf("## ESCOPO LOCAL ##\n");
			printf("Funcao do escopo local: %s\n",aux->chave_funcao);
			dicionario_imprime(aux->escopo_local);
			printf("## ##\n");
			aux= aux->prox;
		}
	}
}
