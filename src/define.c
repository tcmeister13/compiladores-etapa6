/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include "define.h"
#include <string.h>

/**
 * Converte a operação IKS para o seu opCode correspondente
 * name: IKS_AST_para_opCode
 * @param ast: operação a ser convertida
 * @return opCode
 *
 */
char* IKS_AST_para_opCode(int ast)
{
	switch(ast)
	{
		case IKS_AST_ARIM_SOMA    : return "add";            
		case IKS_AST_ARIM_SUBTRACAO  : return "sub";         
		case IKS_AST_ARIM_MULTIPLICACAO : return "mul";      
		case IKS_AST_ARIM_DIVISAO     : return "div";       
		case IKS_AST_ARIM_INVERSAO	: return "sub";
		case IKS_AST_LOGICO_E: return "and";
		case IKS_AST_LOGICO_OU: return "or";
		case IKS_AST_LOGICO_COMP_DIF: return "comp_NE";
		case IKS_AST_LOGICO_COMP_IGUAL: return "comp_EQ";
		case IKS_AST_LOGICO_COMP_LE: return "comp_LE";
		case IKS_AST_LOGICO_COMP_GE: return "comp_GE";
		case IKS_AST_LOGICO_COMP_L: return "comp_LT";
		case IKS_AST_LOGICO_COMP_G: return "comp_GT";
		case IKS_AST_LOGICO_COMP_NEGACAO: return "xor";break;

		default: return "";
	}
}


/**
 * Retorna número de filhos próprios de um tipo de arvore
 * name: FILHOS_AST
 * @param id: tipo arvore
 * @return numero de filhos do tipo arvore
 *
 */
int FILHOS_AST(int id)
{
	switch(id)
		{
			case IKS_AST_PROGRAMA : return 1;            
			case IKS_AST_FUNCAO  : return 1;    
			case IKS_AST_IF_ELSE  : return 3;                
			case IKS_AST_DO_WHILE  : return 2;                
			case IKS_AST_WHILE_DO : return 2;                 
			case IKS_AST_INPUT : return 1;                    
			case IKS_AST_OUTPUT : return 1;                   
			case IKS_AST_ATRIBUICAO  : return 2;              
			case IKS_AST_RETURN    : return 1;                
			case IKS_AST_BLOCO   : return 1;                  
			case IKS_AST_IDENTIFICADOR  : return 0;         
			case IKS_AST_LITERAL     : return 0;           
			case IKS_AST_ARIM_SOMA    : return 2;            
			case IKS_AST_ARIM_SUBTRACAO  : return 2;         
			case IKS_AST_ARIM_MULTIPLICACAO : return 2;      
			case IKS_AST_ARIM_DIVISAO     : return 2;        
			case IKS_AST_ARIM_INVERSAO    : return 1;         // - (operador unário -)
			case IKS_AST_LOGICO_E       : return 2;           // &&
			case IKS_AST_LOGICO_OU     : return 2;           // ||
			case IKS_AST_LOGICO_COMP_DIF   : return 2;       // !=
			case IKS_AST_LOGICO_COMP_IGUAL  : return 2;       // ==
			case IKS_AST_LOGICO_COMP_LE    : return 2;       // <=
			case IKS_AST_LOGICO_COMP_GE   : return 2;         // >=
			case IKS_AST_LOGICO_COMP_L      : return 2;       // <
			case IKS_AST_LOGICO_COMP_G     : return 2;        // >
			case IKS_AST_LOGICO_COMP_NEGACAO  : return 1;     // !
			case IKS_AST_VETOR_INDEXADO     : return 2;       // para var[exp] quando o índice exp é acessado no vetor var
			case IKS_AST_CHAMADA_DE_FUNCAO   : return 2;     

		}
	}

/**
 * Retorna tamanho de um elemento, dado seu tipo
 * name: TAMANHO_POR_TIPO
 * @param tipo: tipo do elemento
 * @return tamanho do tipo do elemento
 *
 */
int TAMANHO_POR_TIPO(int tipo)
{
	switch(tipo)
	{
		case IKS_INT: return TAMANHO_INT;
		case IKS_FLOAT: return TAMANHO_FLOAT;
		case IKS_BOOL: return TAMANHO_BOOL;
		case IKS_CHAR: return TAMANHO_CHAR;
		case IKS_STRING: return TAMANHO_CHAR;
		default: return 0;
	}
}

/**
 * Converte id para texto
 * name: TIPO_ID_para_texto
 * @param t_id: tipo do id a ser convertido
 * @return String correspondente ao tipo de id
 *
 */
char* TIPO_ID_para_texto(int t_id)
{
	switch(t_id)
	{
		case TIPO_ID_FUNCAO: return "id funcao";
		case TIPO_ID_VETOR: return "id vetor";
		case TIPO_ID_VARIAVEL: return "id variavel";
		default: return "nao id";
	}
	
}
/**
 * Retorna string compatível para o define de um tipo AST
 * name: IKS_AST_para_texto
 * @param iks: tipo AST
 * @return texto correspondente ao tipo AST
 *
 */
char* IKS_AST_para_texto(int iks)
{
	switch(iks)
	{
		case IKS_AST_PROGRAMA : return "IKS_AST_PROGRAMA";            
		case IKS_AST_FUNCAO  : return "IKS_AST_FUNCAO";    
		case IKS_AST_IF_ELSE  : return "IKS_AST_IF_ELSE";                
		case IKS_AST_DO_WHILE  : return "IKS_AST_DO_WHILE";                
		case IKS_AST_WHILE_DO : return "IKS_AST_WHILE_DO";                 
		case IKS_AST_INPUT : return "IKS_AST_INPUT";                    
		case IKS_AST_OUTPUT : return "IKS_AST_OUTPUT";                   
		case IKS_AST_ATRIBUICAO  : return "IKS_AST_ATRIBUICAO";              
		case IKS_AST_RETURN    : return "IKS_AST_RETURN";                
		case IKS_AST_BLOCO   : return "IKS_AST_BLOCO";                  
		case IKS_AST_IDENTIFICADOR  : return "IKS_AST_IDENTIFICADOR";         
		case IKS_AST_LITERAL     : return "IKS_AST_LITERAL";           
		case IKS_AST_ARIM_SOMA    : return "IKS_AST_ARIM_SOMA";            
		case IKS_AST_ARIM_SUBTRACAO  : return "IKS_AST_ARIM_SUBTRACAO";         
		case IKS_AST_ARIM_MULTIPLICACAO : return "IKS_AST_ARIM_MULTIPLICACAO";      
		case IKS_AST_ARIM_DIVISAO     : return "IKS_AST_ARIM_DIVISAO";        
		case IKS_AST_ARIM_INVERSAO    : return "IKS_AST_ARIM_INVERSAO";         // - (operador unário -)
		case IKS_AST_LOGICO_E       : return "IKS_AST_LOGICO_E";           // &&
		case IKS_AST_LOGICO_OU     : return "IKS_AST_LOGICO_OU";           // ||
		case IKS_AST_LOGICO_COMP_DIF   : return "IKS_AST_LOGICO_COMP_DIF";       // !=
		case IKS_AST_LOGICO_COMP_IGUAL  : return "IKS_AST_LOGICO_COMP_IGUAL";       // ==
		case IKS_AST_LOGICO_COMP_LE    : return "IKS_AST_LOGICO_COMP_LE";       // <=
		case IKS_AST_LOGICO_COMP_GE   : return "IKS_AST_LOGICO_COMP_GE";         // >=
		case IKS_AST_LOGICO_COMP_L      : return "IKS_AST_LOGICO_COMP_L";       // <
		case IKS_AST_LOGICO_COMP_G     : return "IKS_AST_LOGICO_COMP_G";        // >
		case IKS_AST_LOGICO_COMP_NEGACAO  : return "IKS_AST_LOGICO_COMP_NEGACAO";     // !
		case IKS_AST_VETOR_INDEXADO     : return "IKS_AST_VETOR_INDEXADO";       // para var[exp] quando o índice exp é acessado no vetor var
		case IKS_AST_CHAMADA_DE_FUNCAO   : return "IKS_AST_CHAMADA_DE_FUNCAO";     
		default: "ERROR IKS_AST";
	}

}

/**
 * Retorna string compatível para o define de um tipo de um no
 * name: IKS_TIPO_para_texto
 * @param iks: tipo de um no
 * @return texto correspondente ao tipo do no
 *
 */
char* IKS_TIPO_para_texto(int iks)
{
	switch(iks)
	{
		case IKS_INT: return "IKS_INT";
		case IKS_FLOAT: return "IKS_FLOAT";
		case IKS_CHAR: return "IKS_CHAR";
		case IKS_STRING: return "IKS_STRING";
		case IKS_BOOL: return "IKS_BOOL";
		case IKS_TIPO_ESPECIAL: return "IKS_TIPO_ESPECIAL";
		default: return "IKS_TIPO error";
		
	}
}


/**
 * Mostra na tela erro correspondente ao tipo conforme linha
 * name: Imprime_Erro
 * @param iks: tipo de um no
 * @return texto correspondente ao tipo do no
 *
 */
void Imprime_Erro (int linha, int erro)
{
	switch (erro){
	
	case IKS_ERROR_UNDECLARED: printf("Identificador não declarado na linha %d \n", linha);
			break;
	case IKS_ERROR_DECLARED: printf("Identificador já declarado na linha %d \n", linha);
			break;
	case IKS_ERROR_VARIABLE: printf("Identificador deve ser usado como variável na linha %d \n", linha);
			break;
	case IKS_ERROR_VECTOR: printf("Identificador deve ser usado como vetor na linha %d \n", linha);
			break;
	case IKS_ERROR_FUNCTION: printf("Identificador deve ser usado como função na linha %d \n", linha);
			break;
	case IKS_ERROR_WRONG_TYPE: printf("Tipos incompativeis na linha %d \n", linha);
			break;
	case IKS_ERROR_STRING_TO_X: printf("Coercao impossivel de string na linha %d \n", linha);
			break;
	case IKS_ERROR_CHAR_TO_X: printf("Coercao impossivel de caracter na linha %d \n", linha);
			break;
	case IKS_ERROR_MISSING_ARGS: printf("Faltam argumentos na linha %d \n", linha);
			break;
	case IKS_ERROR_EXCESS_ARGS: printf("Sobram argumentos na linha %d \n", linha);
			break;
	case IKS_ERROR_WRONG_TYPE_ARGS: printf("Argumentos icompatíveis na linha %d \n", linha);
			break;
	case IKS_ERROR_WRONG_PAR_INPUT: printf("Parametro nao eh identificador na linha %d \n", linha);
			break;
	case IKS_ERROR_WRONG_PAR_OUTPUT: printf("Parametro nao eh literal string ou expressao na linha %d \n", linha);
			break;
	case IKS_ERROR_WRONG_PAR_RETURN: printf("Parametro nao eh compativel com expressao na linha de retorno na linha %d \n", linha);
			break;
	case IKS_ERROR_VETOR_MISSING: printf("Faltam dimensoes para acesso de vetor na linha %d \n", linha); 
			break;
	case IKS_ERROR_VETOR_EXCESS: printf("Sobram dimensoes para acesso de vetor na linha %d \n", linha); 
			break;

		}
	}
	
	
/**
 * Retorna qual tipo de erro de compatibilidade de tipos ocorreu com base no que era para ser convertido
 * name: tipo_coercao_incompativel
 * @param t_convertido: tipo do no que nao pode ser convertido
 * @return erro da coerção
 *
 */
int tipo_coercao_incompativel(int t_convertido)
{
	switch(t_convertido)
	{
		case IKS_CHAR: return IKS_ERROR_CHAR_TO_X;
		case IKS_STRING : return IKS_ERROR_STRING_TO_X;
		default: return IKS_ERROR_WRONG_TYPE;
	}
}

/**
 * Retorna qual tipo de erro de inferencia de tipos ocorreu com base no erro
 * name: tipo_inferencia_incompativel
 * @param t_erro: erro ocorrido na inferencia
 * @return erro correspondente
 *
 */
int tipo_inferencia_incompativel(int t_erro)
{
	switch(t_erro)
	{
		case -1: return IKS_ERROR_CHAR_TO_X;
		case -2 : return IKS_ERROR_STRING_TO_X;
		default: return IKS_ERROR_WRONG_TYPE;
	}
}
