/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comp_pilha.h"

comp_pilha_t* pilha_init(){
    return NULL;
}


comp_pilha_t* pilha_push(comp_pilha_t *p, int info){

    /* Cria novo nodo com INFO */
    comp_pilha_t* novo = malloc(sizeof(comp_pilha_t*));
    novo->info = info;

    /* Insere no primeiro elemento da pilha */
    novo->prox = p;
    return novo;
}


comp_pilha_t* pilha_pop(comp_pilha_t *p){
    comp_pilha_t* ptaux;

    if(p == NULL){
        return NULL;
    }

    ptaux = p->prox;

    free(p);
    return ptaux;
}


void pilha_imprime(comp_pilha_t *p){
    comp_pilha_t* ptaux = p;

    printf("\nTOPO:");
    while(ptaux != NULL){
        printf("\nINFO: %d", ptaux->info);
        ptaux = ptaux->prox;
    }
    printf("\n----\n");

    return;
}

